import { CohereClient, CohereError, CohereTimeoutError } from "cohere-ai";

const cohere = new CohereClient({
    token: "wrtZ5yM5cMohzJJuu7gvQQNTC4ERKRXE9gHNcDqF",
});

(async () => {
    const args = process.argv.slice(2);
    const question = args.join(' ');

    if (!question) {
    console.error('Por favor, proporciona una pregunta.');
    process.exit(1);
    }
    try {
        const response = await cohere.generate({
            model: "command",
            prompt: `Háblame sobre ${question} en el contexto del marketing digital (traducela al español).`,
        });
        console.log("Tu Respuesta: " , response.generations[0].text);
        
    } catch (err) {
        if (err instanceof CohereTimeoutError) {
            console.log("Request timed out", err);
        } else if (err instanceof CohereError) {
            // catch all errors
            console.log(err.statusCode);
            console.log(err.message);
            console.log(err.body);
        }
    }
    
})();