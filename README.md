
# Proyecto de Prueba Técnica

## Descripción

Este repositorio contiene un programa desarrollado en Node.js diseñado para realizar consultas específicas relacionadas con el campo del marketing digital utilizando ChatGPT. La aplicación permite recibir una pregunta como parámetro y utilizar la inteligencia artificial de ChatGPT para generar respuestas centradas exclusivamente en el ámbito del marketing digital.

## Instrucciones de Uso

1. **Instalación:**
   Asegúrate de tener Node.js instalado en tu máquina. Puedes descargarlo desde [nodejs.org](https://nodejs.org/).

2. **Clonación del Repositorio:**
   ```bash
   git clone https://url-del-repositorio.git

3. **Instalación de Dependencias:**
   ```bash
    cd nombre-del-repositorio
    npm install

3. **Ejecución del Programa**
   ```bash
    node cohere.js "¿Cuál es tu pregunta sobre marketing digital?"

Sustituye la pregunta entre comillas con tu consulta específica sobre marketing digital.

## Contribuciones

¡Contribuciones son bienvenidas! Si encuentras errores o deseas mejorar la funcionalidad, no dudes en crear un pull request. ¡Gracias por tu interés en nuestro proyecto!
