import OpenAI from "openai";

const apiKey = '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';
const openai = new OpenAI({ apiKey });

async function main() {
  // Obtener la pregunta desde la línea de comandos
  const args = process.argv.slice(2);
  const pregunta = args.join(' ');

  if (!pregunta) {
    console.error('Por favor, proporciona una pregunta.');
    process.exit(1);
  }

  const completion = await openai.completions.create({
    model: 'gpt-3.5-turbo-instruct', 
    prompt: `Háblame sobre ${pregunta} en el contexto del marketing digital.`,
  });

  console.log(completion.choices[0].text.trim());
}

main();
